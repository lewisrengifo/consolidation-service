package com.bootcamp.events;

import com.bootcamp.infraestructure.entity.Credits;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class CreditsCreatedEvent extends Event<Credits> {}
