package com.bootcamp.events;

public enum EventType {
	CREATED, UPDATED, DELETED
}
