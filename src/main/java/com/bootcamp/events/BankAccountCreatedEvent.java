package com.bootcamp.events;

import com.bootcamp.infraestructure.entity.BankAccounts;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class BankAccountCreatedEvent extends Event<BankAccounts>{
}
