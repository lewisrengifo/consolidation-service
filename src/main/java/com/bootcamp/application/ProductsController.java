package com.bootcamp.application;

import com.bootcamp.infraestructure.document.Products;
import com.bootcamp.infraestructure.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/product")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @PostMapping()
    @ResponseStatus(HttpStatus.CREATED)
    public Mono<Products> saveProducts(@RequestBody Products products){
        return productsService.save(products);
    }
}
