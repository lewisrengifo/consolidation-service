package com.bootcamp.domain;

import com.bootcamp.events.CreditsCreatedEvent;
import com.bootcamp.events.Event;
import com.bootcamp.infraestructure.entity.Credits;

import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class CreditsEventsService extends Event<Credits> {

  @KafkaListener(
      topics = "${topic.credits.name:credits}",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "3")
  public void consumer(Event<?> event) {
    if (event.getClass().isAssignableFrom(CreditsCreatedEvent.class)) {
      CreditsCreatedEvent creditsCreatedEvent = (CreditsCreatedEvent) event;
      log.info(
          "Received credits created event ..... with Id={}, data={}",
          creditsCreatedEvent.getId(),
          creditsCreatedEvent.getData());
    }
  }
}
