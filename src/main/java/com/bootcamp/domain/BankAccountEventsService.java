package com.bootcamp.domain;

import com.bootcamp.events.Event;
import com.bootcamp.events.BankAccountCreatedEvent;
import com.bootcamp.infraestructure.document.Products;
import com.bootcamp.infraestructure.entity.BankAccounts;
import com.bootcamp.infraestructure.entity.CreditCard;
import com.bootcamp.infraestructure.entity.Credits;
import com.bootcamp.infraestructure.repository.ProductsRepository;
import com.bootcamp.infraestructure.service.ProductsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class BankAccountEventsService {
  private ProductsRepository productsRepository;

  public BankAccountEventsService(ProductsRepository productsRepository) {
    this.productsRepository = productsRepository;
  }

  @KafkaListener(
      topics = "${topic.bankAccount.name:bankAccounts}",
      containerFactory = "kafkaListenerContainerFactory",
      groupId = "2")
  public void consumer(Event<?> event) {
    if (event.getClass().isAssignableFrom(BankAccountCreatedEvent.class)) {
      BankAccountCreatedEvent bankAccountCreatedEvent = (BankAccountCreatedEvent) event;
      log.info(
          "Received bankAccount created event .... with Id={}, data={}",
          bankAccountCreatedEvent.getId(),
          bankAccountCreatedEvent.getData().toString());

      BankAccounts bankAccounts = bankAccountCreatedEvent.getData();
      List<Credits> creditsList = new ArrayList<>();
      List<BankAccounts> bankAccountsList = new ArrayList<>();
      bankAccountsList.add(bankAccounts);
      String id = UUID.randomUUID().toString();
      Products products =
          new Products(
              id,
              bankAccounts.getClientDocument(),
              bankAccounts.getClientName(),
              bankAccounts.getTypeOfClient(),
              creditsList,
              bankAccountsList);
      productsRepository.save(products).subscribe();
      log.info("products guardado en la db");
    }
  }
}
