package com.bootcamp.domain;

import com.bootcamp.infraestructure.document.Products;
import com.bootcamp.infraestructure.repository.ProductsRepository;
import com.bootcamp.infraestructure.service.ProductsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Slf4j
@Service
public class ProductsServiceImpl implements ProductsService {

  //@Autowired private ProductsService productsService;

  @Autowired private ProductsRepository productsRepository;

  @Override
  public Flux<Products> getAll() {
    return this.productsRepository.findAll();
  }

  @Override
  public Mono<Products> save(Products products) {
    log.info("ingresando valor: " +products);
    return productsRepository.save(products);
  }

  @Override
  public Mono<Products> findById(String clientId) {
    return null;
  }

  @Override
  public Mono<Products> deleteById(String clientId) {
    return null;
  }
}
