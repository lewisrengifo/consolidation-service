package com.bootcamp.infraestructure.service;

import com.bootcamp.infraestructure.document.Products;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ProductsService {
    Flux<Products> getAll();

    Mono<Products> save(Products products);

    Mono<Products> findById(String clientId);

    Mono<Products> deleteById(String clientId);
}
