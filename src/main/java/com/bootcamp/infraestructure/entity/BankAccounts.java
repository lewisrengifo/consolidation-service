package com.bootcamp.infraestructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

/**
 * Mongo document for bank account.
 */
@Getter
@Setter
@NoArgsConstructor
//@Document(collection = "bankAccounts")
public class BankAccounts {
  private String id;
  private String clientDocument;
  private String clientName;
  private String typeOfBankAccount;
  private String typeOfClient;
  private BigDecimal totalAmount;
  private BigDecimal maintenanceFee;
  private Integer totalTransactionsMonth;
  private List<Holders> holdersList;
}
