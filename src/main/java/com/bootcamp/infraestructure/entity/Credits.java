package com.bootcamp.infraestructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Null;
import java.math.BigDecimal;

/** Mongo document for credits. */
@Getter
@Setter
@NoArgsConstructor
// @Document(collection = "credits")
public class Credits {
  private String id;
  private String clientDocument;
  private BigDecimal totalAmount;
  private BigDecimal creditLimit;
  private String type;
  @Null private CreditCard creditCard;
}
