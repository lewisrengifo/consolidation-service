package com.bootcamp.infraestructure.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/** Credit card entity. */
@Getter
@Setter
@NoArgsConstructor
public class CreditCard {
  private String cardNumber;
  private Date expirationDate;
  private String ccv;
}
