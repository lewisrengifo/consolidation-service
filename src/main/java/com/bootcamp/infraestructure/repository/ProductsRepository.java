package com.bootcamp.infraestructure.repository;

import com.bootcamp.infraestructure.document.Products;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends ReactiveMongoRepository<Products , String> {}
