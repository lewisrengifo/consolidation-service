package com.bootcamp.infraestructure.document;

import com.bootcamp.infraestructure.entity.BankAccounts;
import com.bootcamp.infraestructure.entity.Credits;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Null;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@Document(collection = "products")
public class Products {
  @Id private String id;
  private String clientDocument;
  private String clientName;
  private String clientType;
  private List<Credits> creditsList;
  private List<BankAccounts> bankAccountsList;
}
